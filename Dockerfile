FROM openjdk:8-alpine as builder
RUN mkdir /app
COPY  . /app
WORKDIR /app
RUN chmod +x gradlew
RUN ./gradlew build --no-daemon

FROM openjdk:8-alpine
ENV CRON_RAW=""
RUN mkdir /app
WORKDIR /app
COPY --from=builder /app/build/libs/*.jar application.jar
ENTRYPOINT java -jar ./application.jar "$CRON_RAW"