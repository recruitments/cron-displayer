.DEFAULT_GOAL := help

APP_NAME:=$(shell grep appName ./gradle.properties | cut -d'=' -f2)
APP_VERSION:=$(shell grep appVersion ./gradle.properties | cut -d'=' -f2)
JAR_NAME:=$(APP_NAME)-$(APP_VERSION).jar

CRON_RAW := ""

.PHONY: help
help: ## show help
	@grep -E '^([a-zA-Z_-]|\.)+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: application.clean
application.clean: ## cleans built application
	@chmod +x gradlew
	@./gradlew clean --no-daemon

.PHONY: application.build
application.build: application.clean ## build application
	@./gradlew build --no-daemon

.PHONY: application.run
application.run: ## run application
	@java -jar ./build/libs/$(JAR_NAME) "$(CRON_RAW)"

.PHONY: docker.build
docker.build: ## build docker image
	@docker build -t $(APP_NAME):$(APP_VERSION) .

.PHONY: docker.run
docker.run: ## run application in container
	@docker run -e "CRON_RAW=$(CRON_RAW)" $(APP_NAME):$(APP_VERSION)