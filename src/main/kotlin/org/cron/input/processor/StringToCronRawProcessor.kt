package org.cron.input.processor

import org.cron.input.exception.CronInputInvalid
import org.cron.input.model.CronRaw
import org.cron.utils.Processor
import java.util.regex.Pattern

class StringToCronRawProcessor: Processor<String, CronRaw> {

    companion object {
        private val CRON_PATTERN = Pattern.compile("(([\\,\\/\\-\\*0-9]+\\s+){5})(.*)")
    }

    override fun process(input: String): CronRaw {
        val matcher = CRON_PATTERN.matcher(input)
        if(!matcher.matches()) {
            throw CronInputInvalid(
                cron = input,
                pattern = CRON_PATTERN.pattern()
            )
        }
        val periodString = matcher.group(1).trim().split(" ") //todo: it would be useful to add many spaces trim working
        val commandString = matcher.group(3).trim()
        return CronRaw(
            minute = periodString[0],
            hour = periodString[1],
            dayOfMonth = periodString[2],
            month = periodString[3],
            dayOfWeek = periodString[4],
            command = commandString
        )
    }

}