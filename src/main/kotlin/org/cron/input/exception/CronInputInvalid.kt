package org.cron.input.exception

class CronInputInvalid(
    private val cron: String,
    private val pattern: String
): RuntimeException(
    "Passed cron input string is invalid [$cron]. Pattern used to check validity is: \"$pattern\""
)