package org.cron.input.model

data class CronRaw(
    val minute: String,
    val hour: String,
    val dayOfMonth: String,
    val month: String,
    val dayOfWeek: String,
    val command: String
)