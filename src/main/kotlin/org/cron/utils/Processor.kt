package org.cron.utils

interface Processor<I, O> {

    fun process(input: I): O

}