package org.cron

import org.cron.displayable.DisplayService
import org.cron.displayable.processor.CronRawToCronProcessor
import org.cron.displayable.processor.CronRawToCronProcessor.Companion.ALL_PERIOD_PROCESSORS
import org.cron.input.processor.StringToCronRawProcessor

val displayService = DisplayService()
val stringToCronRawProcessor = StringToCronRawProcessor()
val cronRawToCronProcessor = CronRawToCronProcessor(*ALL_PERIOD_PROCESSORS.toTypedArray())

fun main(args: Array<String>) {
    if (args.size != 1) {
        throw IllegalArgumentException("You should pass only cron entry as a single string!")
    }
    val cronRaw = stringToCronRawProcessor.process(args[0])
    val cron = cronRawToCronProcessor.process(cronRaw)
    displayService.display(cron)
}