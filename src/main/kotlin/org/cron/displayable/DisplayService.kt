package org.cron.displayable

import org.cron.displayable.model.Cron

class DisplayService {

    fun display(cron: Cron) {
        print("""
        minute        ${cron.minute.joinToString(" ")}
        hour          ${cron.hour.joinToString(" ")}
        day of month  ${cron.dayOfMonth.joinToString(" ")}
        month         ${cron.month.joinToString(" ")}
        day of week   ${cron.dayOfWeek.joinToString(" ")}
        command       ${cron.command}
        """.trimIndent()
        )
    }

}