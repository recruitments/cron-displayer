package org.cron.displayable.processor.period.impl

import org.cron.displayable.processor.period.Period
import org.cron.displayable.processor.period.PeriodProcessor
import java.util.regex.Pattern

class SpecificPeriodProcessor: PeriodProcessor {

    companion object {
        private val pattern = Pattern.compile("[0-9]+")
    }

    override fun process(input: Period): List<Int> {
        return listOf(input.raw.toInt())
            .filter { it >= input.min && it <= input.max }
    }

    override fun shouldProcess(input: String): Boolean {
        return pattern.matcher(input).matches()
    }
}