package org.cron.displayable.processor.period

data class Period(
    val raw: String,
    val min: Int,
    val max: Int
)