package org.cron.displayable.processor.period.impl

import org.cron.displayable.processor.period.Period
import org.cron.displayable.processor.period.PeriodProcessor

class AllPeriodProcessor: PeriodProcessor {

    override fun process(input: Period): List<Int> {
        return (input.min .. input.max).toList()
    }

    override fun shouldProcess(input: String): Boolean {
        return input == "*"
    }
}