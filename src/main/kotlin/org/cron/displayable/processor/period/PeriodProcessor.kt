package org.cron.displayable.processor.period

import org.cron.utils.Processor

interface PeriodProcessor: Processor<Period, List<Int>> {

    fun shouldProcess(input: String): Boolean

}