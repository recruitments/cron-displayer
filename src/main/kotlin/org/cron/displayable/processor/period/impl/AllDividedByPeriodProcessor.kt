package org.cron.displayable.processor.period.impl

import org.cron.displayable.processor.period.Period
import org.cron.displayable.processor.period.PeriodProcessor
import java.util.regex.Pattern

class AllDividedByPeriodProcessor: PeriodProcessor {

    companion object {
        private val pattern = Pattern.compile("\\*\\/[0-9]+")
    }

    override fun process(input: Period): List<Int> {
        val divideBy = input.raw.split("/")[1].toInt()
        return (0 .. input.max step divideBy)
            .filter { it >= input.min && it <= input.max }
    }

    override fun shouldProcess(input: String): Boolean {
        return pattern.matcher(input).matches()
    }
}