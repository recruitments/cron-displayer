package org.cron.displayable.processor

import org.cron.displayable.model.Cron
import org.cron.displayable.processor.period.Period
import org.cron.displayable.processor.period.PeriodProcessor
import org.cron.displayable.processor.period.impl.AllDividedByPeriodProcessor
import org.cron.displayable.processor.period.impl.AllPeriodProcessor
import org.cron.displayable.processor.period.impl.RangePeriodProcessor
import org.cron.displayable.processor.period.impl.SpecificPeriodProcessor
import org.cron.input.model.CronRaw
import org.cron.utils.Processor

class CronRawToCronProcessor(
    private vararg val processors: PeriodProcessor
): Processor<CronRaw, Cron> {

    companion object {
        val ALL_PERIOD_PROCESSORS = listOf(
            AllDividedByPeriodProcessor(),
            AllPeriodProcessor(),
            RangePeriodProcessor(),
            SpecificPeriodProcessor()
        )
    }

    override fun process(input: CronRaw): Cron {
        return Cron(
            minute = periodPatternToOccurrences(input.minute, 0, 59),
            hour = periodPatternToOccurrences(input.hour, 0, 23),
            dayOfMonth = periodPatternToOccurrences(input.dayOfMonth, 1, 31),
            month = periodPatternToOccurrences(input.month, 1, 12),
            dayOfWeek = periodPatternToOccurrences(input.dayOfWeek, 1, 7),
            command = input.command
        )
    }

    private fun periodPatternToOccurrences(
        raw: String,
        min: Int,
        max: Int
    ): List<Int> {
        val periods = raw
            .split(",")
            .map { Period(it, min, max) }
        return periods
            .flatMap { period ->
                processors
                    .firstOrNull { it.shouldProcess(period.raw) }
                    ?.process(period) ?: listOf()
            }
            .sorted()
    }

}