package org.cron.input.processor

import org.cron.input.exception.CronInputInvalid
import org.cron.input.model.CronRaw
import org.cron.stub.CronRawStub
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class StringToCronRawProcessorTest {

    private val stringToCronRawProcessor = StringToCronRawProcessor()

    @Test
    fun `process valid cron string should return CronRaw object`() {
        //when
        val actual = stringToCronRawProcessor.process(CronRawStub.cronString())

        //then
        assertEquals(CronRawStub.cronRaw(), actual)
    }

    @Test
    @Disabled
    fun `process valid cron string with extra spaces should return CronRaw object with additional spaces removed`() {
        //given
        val validCronStringWithExtraSpaces = "*/15  0            1,15 * 1-5 /usr/bin/find  "

        //when
        val actual = stringToCronRawProcessor.process(validCronStringWithExtraSpaces)

        //then
        assertEquals(CronRawStub.cronRaw(), actual)
    }

    @Test
    fun `process invalid cron string should throw exception`() {
        //given
        val invalidCronString = ""

        //when && then
        assertThrows<CronInputInvalid> {
            stringToCronRawProcessor.process(invalidCronString)
        }
    }

}