package org.cron.stub

import org.cron.displayable.model.Cron

object CronStub {

    fun cron() = Cron(
        minute = listOf(0, 15, 30, 45),
        hour = listOf(0),
        dayOfMonth = listOf(1, 15),
        month = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
        dayOfWeek = listOf(1, 2, 3, 4, 5),
        command = "/usr/bin/find"
    )

}