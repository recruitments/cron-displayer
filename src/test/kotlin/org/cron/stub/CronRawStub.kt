package org.cron.stub

import org.cron.input.model.CronRaw

object CronRawStub {

    fun cronRaw() = CronRaw(
        minute = "*/15",
        hour = "0",
        dayOfMonth = "1,15",
        month = "*",
        dayOfWeek = "1-5",
        command = "/usr/bin/find"
    )

    fun cronString() = "*/15 0 1,15 * 1-5 /usr/bin/find"

}