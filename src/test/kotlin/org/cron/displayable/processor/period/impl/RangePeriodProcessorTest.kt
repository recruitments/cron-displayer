package org.cron.displayable.processor.period.impl

import org.cron.displayable.processor.period.Period
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class RangePeriodProcessorTest {

    val processor = RangePeriodProcessor()

    @Nested
    inner class Process {

        @Test
        fun `should return all minutes`() {
            //given
            val period = Period("1-10", 1, 5)
            val expected = listOf(1,2,3,4,5)

            //whem
            val actual = processor.process(period)

            //then
            assertEquals(expected, actual)
        }

    }

    @Nested
    inner class ShouldProcess {

        @Test
        fun `return true when should process`() {
            //given
            val allPeriod = "1-5"

            //when
            val actual = processor.shouldProcess(allPeriod)

            //then
            assertTrue(actual)
        }

        @ParameterizedTest
        @ValueSource(strings = ["*", "*/4", "1", "1,2,3"])
        fun `return false when should not process`(period: String) {

            //when
            val actual = processor.shouldProcess(period)

            //then
            assertFalse(actual)
        }

    }

}