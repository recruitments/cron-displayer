package org.cron.displayable.processor.period.impl

import org.cron.displayable.processor.period.Period
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class AllPeriodProcessorTest {

    val processor = AllPeriodProcessor()

    @Nested
    inner class Process {

        @Test
        fun `should return all minutes`() {
            //given
            val period = Period("*", 0, 59)

            //when
            val actual = processor.process(period)

            //then
            assertEquals(60, actual.size)
        }

    }

    @Nested
    inner class ShouldProcess {

        @Test
        fun `return true when should process`() {
            //given
            val allPeriod = "*"

            //when
            val actual = processor.shouldProcess(allPeriod)

            //then
            assertTrue(actual)
        }

        @ParameterizedTest
        @ValueSource(strings = ["*/3", "1-3", "1", "1,2,3"])
        fun `return false when should not process`(period: String) {

            //when
            val actual = processor.shouldProcess(period)

            //then
            assertFalse(actual)
        }

    }

}