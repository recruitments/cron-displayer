package org.cron.displayable.processor

import org.cron.displayable.processor.period.impl.AllDividedByPeriodProcessor
import org.cron.displayable.processor.period.impl.AllPeriodProcessor
import org.cron.displayable.processor.period.impl.RangePeriodProcessor
import org.cron.displayable.processor.period.impl.SpecificPeriodProcessor
import org.cron.stub.CronRawStub
import org.cron.stub.CronStub
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class CronRawToCronProcessorTest {

    private val cronRawToCronProcessor = CronRawToCronProcessor(
        AllDividedByPeriodProcessor(),
        AllPeriodProcessor(),
        RangePeriodProcessor(),
        SpecificPeriodProcessor()
    )

    @Test
    fun `should create properly cron info`() {
        //Given
        val cronRaw = CronRawStub.cronRaw()

        //When
        val actual = cronRawToCronProcessor.process(cronRaw)

        //Then
        assertEquals(CronStub.cron(), actual)
    }

}