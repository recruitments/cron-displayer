package org.cron

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class MainKtTest {

    @Test
    fun `should throw exception when invalid arguments provided`() {
        //When && Then
        assertThrows<IllegalArgumentException> {
            main(listOf<String>().toTypedArray())
        }
    }

}