# Cron Displayer
## What this is used for?
#### This application takes cron in raw string as a program input and returns cron in human readable form.

## How can I run this?

1. Container
   1. There is script to run this using container. If you have make installed on your system you can just type those commands:
      1. Build docker image (only first time after changes in project)
      `make docker.build`
      2. Run docker container
      `make docker.run CRON_RAW="[YOUR CRON RAW]"`
   2. If you don't have make installed you can create docker image by yourself. To do that:
      1. `docker build -t [image_name]:[image_tag] .`
      2. `docker run -e "CRON_RAW=[YOUR CRON RAW]" [image_name]:[image_tag]`
2. Using Jar
   1. There is script to run this using jar file. If you have make installed on your system you can just type those commands:
      1. Build jar file
      `make application.build`
      2. Run jar file
      `make application.run CRON_RAW="[YOUR CRON RAW]"`
   2. If you don't have make installed you can create jar file by yourself. To do that:
      1. `./gradlew build`
      2. `java -jar ./build/libs/[JAR_NAME].jar "[YOUR CRON RAW]"`

## Technology stack

1. To develop this application I was using `Kotlin 1.7.21` with jvm compilation target set to `Java 1.8`.
2. Tests were written using `JUnit` with additional library to make some tests as parametrized
3. To make running application easier there was `make` used to make scripts easier to understand or extend them. 
4. Application can be run by docker (containerized environment) or locally using Jar file or IDE. In case of docker there is only one tool needed -> docker. In case of other we must have at least Java 1.8 jdk with kotlin sdk.

## Requirements

1. Container environment
   1. Docker
2. Local development
   1. Java 1.8
   2. Kotlin